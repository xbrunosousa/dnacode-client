const ProductMainDetails = ({ product }) => {
  return (
    <div>
      <h1>{product.name}</h1>
      <hr />
      <img src={product.image} alt={`${product.name} images`} />
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Hic corporis
        illo impedit fugit tempore aperiam, qui explicabo mollitia possimus
        earum quis recusandae expedita architecto modi alias. Quo suscipit
        libero amet.
      </p>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Hic corporis
        illo impedit fugit tempore aperiam, qui explicabo mollitia possimus
        earum quis recusandae expedita architecto modi alias. Quo suscipit
        libero amet.
      </p>
    </div>
  );
};

export default ProductMainDetails;
