import { makeStyles } from '@material-ui/core/styles';
import { IconButton, Typography, Toolbar, AppBar } from '@material-ui/core';
import { Home as HomeIcon } from '@material-ui/icons';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  homeButton: {
    marginRight: theme.spacing(2),
    color: 'white'
  },
  title: {
    flexGrow: 1
  }
}));

const ApplicationAppBar = () => {
  const classes = useStyles();

  return (
    <div className={classes.root} id="appbar">
      <AppBar position='static'>
        <Toolbar>
          <Link to='/'>
            <IconButton
              edge='start'
              className={classes.homeButton}
              color='secondary'
              aria-label='menu'
            >
              <HomeIcon />
            </IconButton>
          </Link>
          <Typography variant='h6' className={classes.title}>
            DNACode Shop
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default ApplicationAppBar;
