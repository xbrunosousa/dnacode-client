import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button
} from '@material-ui/core';
import { formatCurrency } from '../utils';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  form: {
    width: '100%'
  },
  select: {
    width: '100%'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
    fontWeight: theme.typography.fontWeightRegular
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  totalWithInterest: {
    color: theme.palette.text.primary,
    textAlign: 'right',
    fontStyle: 'italic',
    display: 'block',
    paddingTop: 5
  },
  btnBuyContainer: {
    textAlign: 'center',
    marginTop: 25
  }
}));

const ProductDetailsPrice = ({ parcels, price }) => {
  const [state, setState] = React.useState({
    selectedParcels: '',
    selectedParcelDetails: {}
  });

  const updateState = (target, value) => {
    setState(prev => ({ ...prev, [target]: value }));
  };

  const getParcelDetails = qtt => {
    const selected = parcels.find(x => x.quantity === qtt);
    updateState('selectedParcelDetails', selected);
  };

  React.useEffect(() => {
    console.log('state.selectedParcels', state.selectedParcels);
    if (state.selectedParcels !== '') {
      getParcelDetails(state.selectedParcels);
    } else {
      updateState('selectedParcelDetails', {});
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.selectedParcels]);

  const classes = useStyles();

  const onSelectParcel = e => {
    const { value } = e.target;
    updateState('selectedParcels', value);
  };

  const selectLabel =
    'Ou selecione a quantidade de parcelas (Juros de 0.45% a.m.)';

  return (
    <div id='product-details-price' className={classes.root}>
      <h1 className='price-total'>{formatCurrency(price)} à vista</h1>
      {/* <hr /> */}
      <h3 className='with-lines'>
        <span>OU</span>
      </h3>
      <FormControl variant='outlined' className={classes.form}>
        <InputLabel id='select-parcels-label'>{selectLabel}</InputLabel>
        <Select
          labelId='select-parcels-label'
          id='select-parcels'
          value={state.selectedParcels}
          onChange={onSelectParcel}
          label={selectLabel}
        >
          <MenuItem value=''>
            <em>-- Selecione a quantidade de parcelas --</em>
          </MenuItem>
          {parcels.map(
            i =>
              i.quantity > 1 && (
                <MenuItem key={i.quantity} value={i.quantity}>
                  {`1 + ${i.quantity - 1}x de ${formatCurrency(i.amount)}`}
                </MenuItem>
              )
          )}
        </Select>
      </FormControl>

      {!!state.selectedParcelDetails?.total && (
        <small className={classes.totalWithInterest}>
          (Total à prazo: {formatCurrency(state.selectedParcelDetails.total)})
        </small>
      )}

      <div className={classes.btnBuyContainer}>
        <Button color='primary' variant='contained' style={{ marginRight: 20 }}>
          Comprar agora
        </Button>
        <Button color='secondary' variant='contained'>
          Adicionar ao carrinho
        </Button>
      </div>
    </div>
  );
};

export default ProductDetailsPrice;
