import axios from 'axios';

const { REACT_APP_CORE_URL: baseURL } = process.env;

export default axios.create({ baseURL });