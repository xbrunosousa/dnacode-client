export const formatCurrency = (value, prefix = 'BRL') => {
  const locale = {
    BRL: 'pt-BR',
    EUR: 'de-DE',
    USD: 'en-IN',
  };

  const getFlooredFixed = (value, digits = 2) => {
    return (
      Math.floor(value * Math.pow(10, digits)) / Math.pow(10, digits)
    ).toFixed(digits);
  };


  return Intl.NumberFormat(locale[prefix], {
    style: 'currency',
    currency: prefix,
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  }).format(getFlooredFixed(value));
}

export const setPageTitle = title => {
  document.title = `${title} | ${process.env.REACT_APP_NAME}`;
}