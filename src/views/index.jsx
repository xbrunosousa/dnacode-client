import React from 'react';
import { Container, Grid } from '@material-ui/core';
import api from './../services/api';
import ProductCard from '../components/ProductCard';
import { setPageTitle } from '../utils';

const Home = () => {
  const [state, setState] = React.useState({
    products: [],
    loading: true
  });

  const getProducts = () => {
    api
      .get('/products')
      .then(res => {
        console.log(res.data);
        if (res.data.success) {
          setState(prev => ({ ...prev, products: res.data.data }));
        }
      })
      .catch(err => console.error(err))
      .finally(() => setState(prev => ({ ...prev, loading: false })));
  };

  React.useEffect(() => {
    setPageTitle('Home');
    getProducts();
  }, []);

  return (
    <Container>
      <Grid container spacing={3}>
        {state.products.map(product => (
          <Grid key={product.id} item xs={4} style={{ marginBottom: 30 }}>
            <ProductCard data={product} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Home;
