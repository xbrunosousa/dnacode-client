import { useState, useEffect } from 'react';
import { Container, Grid } from '@material-ui/core';
import ProductDetailsPrice from '../components/ProductDetailsPrice';
import api from '../services/api';
import { setPageTitle } from '../utils';
import ProductMainDetails from '../components/ProductMainDetails';

const Product = ({ match }) => {
  const [state, setState] = useState({
    loading: true,
    product: {},
    parcels: []
  });

  const updateState = (target, value) => {
    setState(prev => ({ ...prev, [target]: value }));
  };

  const getProduct = () => {
    const { code } = match?.params;
    console.log(code);
    api
      .get(`/products/${code}`)
      .then(res => {
        console.log(res.data);
        if (res.data.success) {
          updateState('product', res.data.data?.product);
          updateState('parcels', res.data.data?.parcels);
        } else {
          alert('[API] Request error');
        }
      })
      .catch(err => console.error(err))
      .finally(() => updateState('loading', false));
  };

  useEffect(() => {
    getProduct();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (state.product.name) setPageTitle(state.product.name);
  }, [state.product.name]);

  if (state.loading) return <p>Carregando...</p>;

  return (
    <Container>
      <Grid
        container
        spacing={3}
        alignContent='center'
        justify='center'
        alignItems='center'
      >
        <Grid item xs={6}>
          <ProductMainDetails product={state.product} />
        </Grid>
        <Grid item xs={6}>
          <ProductDetailsPrice
            price={state.product?.value}
            parcels={state.parcels}
          />
        </Grid>
      </Grid>
    </Container>
  );
};

export default Product;
