import Home from '../views';
import Product from '../views/Product';

const routes = [
  {
    path: '/',
    component: Home,
    exact: true
  },
  {
    path: '/product/:code',
    component: Product,
    exact: true
  }
];

export default routes;