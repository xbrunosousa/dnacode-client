import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import routes from './routes';
import AppBar from './components/AppBar';
import './styles/index.scss';

ReactDOM.render(
  <BrowserRouter>
    <AppBar />
    <Switch>
      { routes.map(i => <Route key={ i.path } { ...i } />) }
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
