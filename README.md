## Client DNACode Shop

## Demo
* É possível visualizar uma demonstração da API em produção [aqui](http://colar_url_aqui.com).

## Instalação

#### Requisitos
* Node v14+;
* Npm ou Yarn


### Passos para executar localmente
- Em seu terminal, na pasta do projeto, execute o comando abaixo:
```sh
$ npm i
```

- Duplique o arquivo `.env.example` e nomeie-o como `.env`
```sh
$ cp .env.example .env
```

- Lembre-se de ajustar o conteúdo do arquivo .env, como a porta em que está rodando o back-end da aplicação.

- Inicie o servidor:
```sh
$ npm run start
```
- A aplicação poderá ser visualizada na URL abaixo:

```sh
http://127.0.0.1:3000
```
